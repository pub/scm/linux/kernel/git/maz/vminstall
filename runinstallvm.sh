#!/bin/bash

OUT=${KEEPME-tmp}
size=${SIZE-16G}
template=${TEMPLATE-/dev/null}

mkdir -p $OUT || exit 1

export DISK=$(mktemp --tmpdir=$OUT XXXXXXXXXXXXXXXX.img)
export EFIVAR=$OUT/$(basename $DISK .img).fd

truncate -s $size $DISK
dd if=$template of=$DISK bs=1M conv=notrunc status=none

truncate -s 64M $EFIVAR

if [ ! -z "$KEEPME" ]; then
    (cat <<EOF
#!/bin/bash

EOF
     export -p
     cat <<EOF
$PWD/runefivm.sh $*
EOF
    ) >"$KEEPME"/runvm.sh
    chmod a+x "$KEEPME"/runvm.sh
fi

./runefivm.sh $*

if [ -z "$KEEPME" ]; then
    rm -f $DISK $EFIVAR
fi
