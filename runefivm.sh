#!/bin/bash

efi=${EFI-/usr/share/AAVMF/AAVMF_CODE.fd}
efivar=${EFIVAR-none}
disk=${DISK-none}
bus=${BUS-pci}
netif=${NETIF}
gic=${GIC-host}
bit=${BIT-64}
cpus=${CPUS-4}
ram=${RAM-1G}
cdrom=${CDROM-nocdimage}
qemu=${QEMU-/usr/bin/qemu-system-aarch64}
accel=${ACCEL-kvm}
cputype=${CPUTYPE-host}
dumpdtb=${DUMPDTB}
withdtb=${WITHDTB}

nifidx=0
tapfd=128
tapredir=""
vhredir=""
netdev=""

build_netdev()
{
    nif=$1
    if [ -e /dev/tap$(< /sys/class/net/$nif/ifindex) ]; then
	addr=$(< /sys/class/net/$nif/address)
	tap=/dev/tap$(< /sys/class/net/$nif/ifindex)
	mq=$((`ls -1 /sys/class/net/$nif/queues/ | wc -l ` / 2))

	tapfds="$(($tapfd))"
	tapredir+="$(($tapfd))<>$tap "
	vhfds="$(($tapfd+$mq))"
	vhredir+="$(($tapfd+$mq))<>/dev/vhost-net "

	for i in `seq 1 $(($mq-1))`
	do
	    tapfds+=":$((i+$tapfd))"
	    tapredir+="$((i+$tapfd))<>$tap "
	    vhfds+=":$((i+$tapfd+$mq))"
	    vhredir+="$((i+$tapfd+$mq))<>/dev/vhost-net "
	done

	netdev+="-netdev tap,fds=$tapfds,id=hostnet$nifidx,vhost=on,vhostfds=$vhfds \
		-device virtio-net-$bus$virtio,mac=$addr,netdev=hostnet$nifidx,mq=on,vectors=$((2*$mq+2)) "

    else
	mq=$((`ls -1 /sys/class/net/$nif/queues/ | wc -l ` / 2))
	mq=4
	vectors="mq=on,vectors=$((2*$mq+2))"
	suffix=`echo $disk | md5sum | sed -e 's/^\(..\)\(..\)\(..\)\(.*\)/\1:\2:\3/'`
	netdev="-netdev tap,vhost=on,queues=$mq,id=hostnet$nifidx,ifname=$nif,script=no,downscript=no \
		-device virtio-net-$bus$virtio,netdev=hostnet$nifidx,mac=5a:fe:00:$suffix,$vectors"
    fi

    tapfd=$(($tapfd + $mq + 1))
    nifidx=$(($nifidx + 1))
}

if [ "$bit" = 64 ]; then
	sf=on
else
	sf=off
fi

qvers=$($qemu --version| grep version | cut -f4 -d' ' | cut -f1,2 -d.)
qversmaj=$(echo $qvers | cut -f1 -d.)
qversmin=$(echo $qvers | cut -f2 -d.)

# Disable virtio legacy if < 2.7
if [ $qversmaj -eq 2 -a $qversmin -ge 7 ]; then
    if [ $bus = pci ]; then
	virtio=",disable-legacy=on"
    fi
fi

if [ $efivar != none ]; then
    efivarpflash="-drive if=pflash,format=raw,file=$efivar"
fi

if [ "$cdrom" != nocdimage ]; then
	cddev="-drive file=$cdrom,id=cdrom,if=none,media=cdrom	\
	       -device virtio-scsi-$bus$virtio			\
	       -device scsi-cd,drive=cdrom"
fi

if [ "$disk" != none ]; then
	dskdev="-drive if=none,format=raw,cache=none,aio=native,file=$disk,id=disk0 \
		-device virtio-blk-$bus$virtio,drive=disk0"
fi

if [ "$netif" == none ]; then
    netdev="-net none"
elif [ -z "$netif" -o $bus != pci ]; then
    netdev="-netdev user,id=hostnet0 -device virtio-net-$bus$virtio,netdev=hostnet0"
else
    IFS=':' read -ra nifs <<< "$netif"
    for i in "${nifs[@]}"; do
	build_netdev $i
    done
fi

args="  -m $ram -smp $cpus -cpu $cputype,aarch64=$sf -machine virt,accel=$accel,gic-version=$gic \
	-nographic \
	-drive if=pflash,format=raw,readonly=on,file=$efi $efivarpflash \
	$netdev \
	$dskdev \
	$cddev \
	$* \
	$tapredir $vhredir \
"

if [ ! -z "$dumpdtb" ]; then
	eval "$qemu -machine dumpdtb=$dumpdtb $args"
	exit $?
fi
	
dtb=`mktemp -p tmp --suffix=.dtb`
dts=`mktemp -p tmp --suffix=.dts`

dtc -q -I fs /sys/firmware/devicetree/base/ | grep fsl,erratum-a008585 && (
echo Enabling erratum for fsl,erratum-a008585

eval "$qemu -machine dumpdtb=$dtb $args"
dtc -I dtb $dtb > $dts

cat <<EOF >>$dts
&{/timer} {
	fsl,erratum-a008585;
};
EOF

dtc -I dts -O dtb -o $dtb $dts
) && forcedtb="-dtb $dtb"

if [ ! -z "$withdtb" ]; then
	forcedtb="-dtb $withdtb"
fi

rm $dts
eval "$qemu $forcedtb $args"
rm $dtb
